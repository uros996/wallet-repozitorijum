<?php
    namespace App\Controllers;

    use App\Core\UserController;
    use App\Models\CashboxModel;
    use App\Models\RecordModel;
    use App\Models\CategoryModel;


    class UserCashboxController extends UserController {
        public function cashboxes() {
            $userId =$this->getSession()->get('userId');
            $cm = new CashboxModel($this->getDatabaseConnection());
            $items = $cm->getAllCategoriesByUserIdFun($userId);
            
            $this->set('cashboxes', $items);
        }
        
        public function getAdd() {
            
        }
    
        public function postAdd() {
            $name = filter_input(INPUT_POST, 'cashbox_name', FILTER_SANITIZE_STRING);
            $cashboxDescription = filter_input(INPUT_POST, 'cashbox_description', FILTER_SANITIZE_STRING);
            $currency = filter_input(INPUT_POST, 'cashbox_currency', FILTER_SANITIZE_STRING);
            $startingSaldo = 0;
            $userId = $this->getSession()->get('userId');
            $cashboxPriority = 0;

            $cm = new CashboxModel($this->getDatabaseConnection());
            

            $cashboxId = $cm->add([
                'name' => $name,
                'description' => $cashboxDescription,
                'priority' => $cashboxPriority,
                'user_id' => $userId,
                'currency' => $currency,
                'saldo' => $startingSaldo
            ]);
            
            if (!$cashboxId) {
                $this->set('message', 'Došlo je do greške prilikom dodavanja nove kase.');
                return;
            }
    
            \ob_clean();
            header('Location: ' . BASE . 'user/cashboxes');
            exit;
        }
    
        public function getEdit($id) {
            $cm = new CashboxModel($this->getDatabaseConnection());
            $userId = $this->getSession()->get('userId');

            $cashbox = $cm->getByIdandUserId($id, $userId);
    
            if (!$cashbox) {
                \ob_clean();
                header('Location: ' . BASE . 'user/cashboxes');
                exit;
            }
    
            $this->set('cashbox', $cashbox);
            
        }
    
        public function postEdit($id) {
            $this->getEdit($id);
    
            $name = filter_input(INPUT_POST, 'cashbox_name', FILTER_SANITIZE_STRING);
            $cashboxDescription = filter_input(INPUT_POST, 'cashbox_description', FILTER_SANITIZE_STRING);

            $cm = new CashboxModel($this->getDatabaseConnection());
    
            $res = $cm->editById($id, [
                'name' => $name,
                'description' => $cashboxDescription
            ]);
            
            if (!$res) {
                $this->set('message', 'Some error occur during updating cashbox data.');
                return;
            }
    
            \ob_clean();
            header('Location: ' . BASE . 'user/cashboxes/detail/'. $id);
            exit;
        }

        public function getDetail(int $id) {
            $cm = new CashboxModel($this->getDatabaseConnection());
            
            $userId = $this->getSession()->get('userId');
            $records = [];
            
            $cashbox = $cm->getByIdandUserId($id, $userId);
            $cm2 = new CategoryModel($this->getDatabaseConnection());
            $categories = $cm2->getAllCategoriesByUserId($userId);
            $rm = new RecordModel($this->getDatabaseConnection());
            $records = $rm->getAllByCashboxId($id);

            if (!$cashbox) {
                \ob_clean();
                header('Location: ' . BASE . 'user/cashboxes');
                exit;
            }
            
            $this->set('categories', $categories);
            $this->set('cashbox', $cashbox);
            $this->set('records', $records);
        }
    }
    