<?php
namespace App\Controllers;

use App\Models\CategoryModel;
use App\Core\Controller;
use App\Models\UserModel;
use App\Validators\StringValidator;

class MainController extends Controller {
    // public function home() {
    //     $userId =$this->getSession()->get('userId');
    //     $cm = new CategoryModel($this->getDatabaseConnection());
    //     $items = $cm->getAllCategoriesByUserId($userId);
    //     $this->set('categories', $items);
    //     // $cm = new CategoryModel($this->getDatabaseConnection());
    //     // $categories = $cm->getAll();
    //     // $this->set('categories', $categories);
    // }

    // public function categoriesSortedById() {
    //     $cm = new CategoryModel($this->getDatabaseConnection());
    //     $categories = $cm->getAll();

    //     usort($categories, function($a, $b) {
    //         return $b->name <=> $a->name;
    //     });

    //     $this->set('categories', $categories);
    // }

    // public function showCategory($categoryId) {
    //     $cm = new CategoryModel($this->getDatabaseConnection());
    //     $category = $cm->getById($categoryId);
    //     $this->set('category', $category);
    // }

    public function loginGet() {

    }

    public function loginPost() {
        $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

        $um = new UserModel($this->getDatabaseConnection());

        $user = $um->getByFieldName('username', $username);

        if (!$user) {
            sleep(1);
            $this->set('message', 'Invalid data!');
            return;
        }

        if (!password_verify($password, $user->password_hash)) {
            sleep(1);
            $this->set('message', 'Invalid data!');
            return;
        }

        $this->getSession()->put('userId', $user->user_id);

        \ob_clean();
        header('Location: ' . BASE . 'user/cashboxes/');
        exit;
    }

    public function logoutPost() {
        $this->getSession()->clear();

        \ob_clean();
        header('Location: ' . BASE . 'user/login');
        exit;
    }

    public function logoutGet() {
        $this->getSession()->remove('userId');
        $this->getSession()->save();

        \ob_clean();
        header('Location: ' . BASE . 'user/login');
        exit;
    }

    public function getRegister() {
        
    }

    public function postRegister() {
        $username  = filter_input(INPUT_POST, 'reg_username', FILTER_SANITIZE_STRING);
        $email     = filter_input(INPUT_POST, 'reg_email', FILTER_SANITIZE_EMAIL);
        $forename  = filter_input(INPUT_POST, 'reg_forename', FILTER_SANITIZE_STRING);
        $surname   = filter_input(INPUT_POST, 'reg_surname', FILTER_SANITIZE_STRING);
        $password1 = filter_input(INPUT_POST, 'reg_password_1', FILTER_SANITIZE_STRING);
        $password2 = filter_input(INPUT_POST, 'reg_password_2', FILTER_SANITIZE_STRING);

        if ($password1 != $password2) {
            $this->set('message', 'Password and re-entered password does not match.');
            return;
        }

        $validator = (new StringValidator())->setMinLength(12)->setMaxLength(120);
        if (! $validator->isValid($password1)) {
            $this->set('message', 'Password has to have at least 12 and the most 120 charatcers.');
            return;
        }

        $um = new UserModel($this->getDatabaseConnection());

        $user = $um->getByFieldName('username', $username);
        if ($user) {
            $this->set('message', 'Username is already taken.');
            return;
        }

        $user = $um->getByFieldName('email', $email);
        if ($user) {
            $this->set('message', 'User with this e-mail already exists.');
            return;
        }

        $passwrodHash = password_hash($password1, PASSWORD_DEFAULT);

        $userId = $um->add([
            'username' =>      $username,
            'password_hash' => $passwrodHash,
            'email' =>         $email,
            'forename' =>      $forename,
            'surname' =>       $surname
        ]);

        if (!$userId) {
            $this->set('message', 'Some error occure during registration.');
            return;
        }

        $this->set('message', 'Registration is succesful.You can login now.');
    }
}
