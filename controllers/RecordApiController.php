<?php
namespace App\Controllers;

use \App\Core\ApiController;
use \App\Models\RecordModel;
use \App\Models\CategoryModel;
use \App\Models\CashboxModel;

class RecordApiController extends ApiController {
    public function getRecords(int $cashboxId) {
        
        $rm = new RecordModel($this->getDatabaseConnection());
        $records = $rm->getAllByCashboxId($cashboxId);
             
        $this->set('records', $records);
        
    }

    public function getIncome(int $cashboxId) {
        $cm = new CategoryModel($this->getDatabaseConnection());
        $rm = new RecordModel($this->getDatabaseConnection());
        $userId = $this->getSession()->get('userId');
        $categories = $rm->getSumByCashboxId($cashboxId, $userId, 'income');
        

        $this->set('parcentage', $categories);
        
    }
    
    public function getOutcome(int $cashboxId) {
        $cm = new CategoryModel($this->getDatabaseConnection());
        $rm = new RecordModel($this->getDatabaseConnection());
        $userId = $this->getSession()->get('userId');
        $categories = $rm->getSumByCashboxId($cashboxId, $userId, 'outcome');
        

        $this->set('parcentage', $categories);
        
    }
    
   
    public function postAdd() {
        $recordCash =  filter_input(INPUT_POST, 'record_value', FILTER_SANITIZE_NUMBER_FLOAT);
        $categoryId = filter_input(INPUT_POST, 'record_category', FILTER_SANITIZE_NUMBER_INT);
        $cashboxId  = filter_input(INPUT_POST, 'cashbox_id', FILTER_SANITIZE_NUMBER_INT);
        $userId = $this->getSession()->get('userId');
        $rm = new RecordModel($this->getDatabaseConnection());
        
        $recordId = $rm->add([
            'cash' => $recordCash,
            'category_id' => $categoryId,
            'cashbox_id' => $cashboxId,
        ]);

        
        if (!$recordId) {
            $this->set('message', 'Some erroor occure when try to add record.');
            return;
        } else {
            $newRecord = $rm->getRecordByRecordId($recordId);
            $this->set('new_record', $newRecord);
        }
    }

    public function getIntervalRecords() {
        $recordCreatedFrom = sprintf('%s 00:00', filter_input(INPUT_POST, 'record_created_from', FILTER_SANITIZE_STRING));
        $recordCreatedTo = sprintf('%s 23:59', filter_input(INPUT_POST, 'record_created_to', FILTER_SANITIZE_STRING));
        
        
        $cashboxId = filter_input(INPUT_POST, 'cashbox_id', FILTER_SANITIZE_NUMBER_INT);
        $rm = new RecordModel($this->getDatabaseConnection());
        $recordsInterval = $rm->getRecordsByTimeScope($cashboxId, $recordCreatedFrom, $recordCreatedTo);

        
        $this->set('created_to', $recordCreatedTo);

        
        $this->set('records_interval', $recordsInterval);

    }
}
