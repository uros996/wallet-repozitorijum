<?php
namespace App\Controllers;

use \App\Core\ApiController;
use \App\Models\CashboxModel;

class CashboxApiController extends ApiController {
    public function getPriorities() {
        $userId =$this->getSession()->get('userId');
        $cm = new CashboxModel($this->getDatabaseConnection());
        $priorities = $cm->getAllCategoriesByUserId($userId);
             
        $this->set('priorities', $priorities);
        
    }

    public function getIncrementPriority($cashboxId) {
        $userId =$this->getSession()->get('userId');
        $cm = new CashboxModel($this->getDatabaseConnection());
        $priority = $cm->getById($cashboxId);
        $newPriority = $priority->priority+1;
        
        $res = $cm->editById($cashboxId, [
            'priority' => $newPriority
        ]);
        if (!$res){
            $this->set('error', 'Not increased!');
            return;
        }
        $this->set('priority', $newPriority);
    }

    public function getDecrementPriority($cashboxId) {
        $userId =$this->getSession()->get('userId');
        $cm = new CashboxModel($this->getDatabaseConnection());
        $priority = $cm->getById($cashboxId);
        $newPriority = $priority->priority - 1;
        $res = $cm->editById($cashboxId, [
            'priority' => $newPriority
        ]);
        if (!$res){
            $this->set('error', 'Not decreased');
            return;
        }
        $this->set('priority', $newPriority);
        
    }


}