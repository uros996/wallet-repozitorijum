<?php
namespace App\Controllers;

use App\Models\CategoryModel;
use App\Core\ApiController;


class MainApiController extends ApiController {
    public function categories() {
        $cm = new CategoryModel($this->getDatabaseConnection());
        $items = $cm->getAll();
        $this->set('categories', $items);
    }
    
}
