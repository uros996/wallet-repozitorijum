<?php

/* Main/postRegister.html */
class __TwigTemplate_5c81cd5d7a05803490ab1ab1c3b52c704431809871f476b03c366916dee24461 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/login.html", "Main/postRegister.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/login.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "    <p>";
        echo twig_escape_filter($this->env, ($context["message"] ?? null));
        echo "</p>

    <p><a href=\"";
        // line 6
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/register/\">Go back on registration page</a></p>
";
    }

    // line 9
    public function block_naslov($context, array $blocks = [])
    {
        // line 10
        echo "Registrtion
";
    }

    public function getTemplateName()
    {
        return "Main/postRegister.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 10,  48 => 9,  42 => 6,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "Main/postRegister.html", "C:\\xampp\\htdocs\\wallet\\views\\Main\\postRegister.html");
    }
}
