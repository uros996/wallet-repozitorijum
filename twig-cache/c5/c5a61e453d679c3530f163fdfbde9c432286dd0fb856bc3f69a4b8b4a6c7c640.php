<?php

/* Main/test.html */
class __TwigTemplate_af5e56765955a1075da01650db10f0ce3a078ccab0ba86fdbc117e2e0b56b3f1 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "Main/test.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, ($context["message"] ?? null));
        echo "
";
    }

    // line 7
    public function block_naslov($context, array $blocks = [])
    {
        // line 8
        echo "Testiranje
";
    }

    public function getTemplateName()
    {
        return "Main/test.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 8,  43 => 7,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "Main/test.html", "C:\\xampp\\htdocs\\wallet\\views\\Main\\test.html");
    }
}
