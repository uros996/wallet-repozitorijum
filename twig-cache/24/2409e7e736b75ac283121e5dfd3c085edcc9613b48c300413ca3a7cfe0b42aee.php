<?php

/* Auction/show.html */
class __TwigTemplate_9ea81e06a6b6b6b4be48c6e3d0a4d9ffa3dfa786f70d68d82508074d4c391c6a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "Auction/show.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "
<button onclick=\"addBookmark(";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "auction_id", []), "html", null, true);
        echo ");\">Add to bookmarks</button>

<h2>";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "title", []));
        echo "</h2>
<p>
    ";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "description", []));
        echo "
</p>
<p>
    Pocinje: ";
        // line 12
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "starts_at", []), "j. n. Y."), "html", null, true);
        echo "<br>
    Zavrsava se: ";
        // line 13
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "ends_at", []), "j. n. Y."), "html", null, true);
        echo "<br>
</p>
<p>
    Trenutna cena: ";
        // line 16
        echo twig_escape_filter($this->env, ($context["lastPrice"] ?? null), "html", null, true);
        echo " din.
</p>
";
    }

    public function getTemplateName()
    {
        return "Auction/show.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 16,  58 => 13,  54 => 12,  48 => 9,  43 => 7,  38 => 5,  35 => 4,  32 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "Auction/show.html", "C:\\xampp\\htdocs\\nedelja09\\views\\Auction\\show.html");
    }
}
