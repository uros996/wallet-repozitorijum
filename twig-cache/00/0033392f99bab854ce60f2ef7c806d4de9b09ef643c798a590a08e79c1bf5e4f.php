<?php

/* UserCategoryManagement/getAdd.html */
class __TwigTemplate_8e24a5755db901c8f5ffccf7038f480da208b450cab614eb19bb0de8b24dbbea extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "UserCategoryManagement/getAdd.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "    <div class=\"card\">
            <div class=\"card-header\">
                <i class=\"fa fa-pencil\"></i>
                Add new category
            </div>

            <div class=\"card-body\">
                <form action=\"";
        // line 11
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/categories/add/\" method=\"POST\" onsubmit=\"return validateCategoryForm()\">
                <div class=\"row\">
                    <div class=\"form-group col col-12 col-md-6\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                                <span class=\"input-group-text\">Name</span>
                            </div>
                            <input type=\"text\" id=\"name\" name=\"name\"
                                class=\"form-control\"
                                required placeholder=\"Enter category name\"
                                pattern=\".*[^\\s]{3,}.*\">
                        </div>
                        <div class=\"alert alert-danger d-none\" id=\"name-error-message\"></div>
                    </div>

                    <div class=\"form-group col col-12 col-md-4\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                                <label class=\"input-group-text\" for=\"select_type\">Type:</label>
                            </div>
                            <select class=\"custom-select\" id=\"select_type\" name=\"type\">
                                    <option value=\"income\">Income</option>
                                    <option value=\"outcome\">Outcome</option>
                                
                            </select>
                        </div>
                    </div>
                    <div class=\"col col-12 col-md-2\"> 
                        <button type=\"submit\" class=\"btn btn-primary pull-right\">
                            <i class=\"fa fa-plus\"></i>
                        </button>
                    </div>
                </div>
                </form>
                
            </div>
        </div>
<script src=\"";
        // line 48
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/js/validators.js\"></script>

";
    }

    // line 52
    public function block_naslov($context, array $blocks = [])
    {
        // line 53
        echo "Add category
";
    }

    public function getTemplateName()
    {
        return "UserCategoryManagement/getAdd.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 53,  92 => 52,  85 => 48,  45 => 11,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "UserCategoryManagement/getAdd.html", "C:\\xampp\\htdocs\\wallet\\views\\UserCategoryManagement\\getAdd.html");
    }
}
