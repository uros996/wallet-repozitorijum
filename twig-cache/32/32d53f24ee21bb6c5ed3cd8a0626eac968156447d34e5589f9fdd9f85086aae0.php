<?php

/* _global/login.html */
class __TwigTemplate_627a3e97251522abe0ea446d06cd52af5a0e4644d0e711a3d8fe160674ee5c24 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'main' => [$this, 'block_main'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <title>
            ";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        // line 6
        echo "        </title>
        <meta charset=\"utf-8\">

        <!-- <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/css/main.css\"> -->
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/libs/bootstrap/dist/css/bootstrap.min.css\">
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/libs/font-awesome/css/font-awesome.min.css\">
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/css/style.css\">
    </head>
    <body>
        <section class=\"container\">
            <header class=\"row justify-content-md-center\" id=\"main-header\">
                <div class=\"col col-12 col-lg-4\">
                    <img alt=\"Banner 1\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/img/site/box1.jpg\" width='350px' height='100px'>
                </div>
                <div class=\"col mr-auto\">
                    <nav class=\"navbar navbar-expand col col-12\">
                        <div class=\"collapse navbar-collapse\" id=\"navbarNavAltMarkup\">
                            <div class=\"navbar-nav ml-auto\">
                                <a class=\"nav-item nav-link active\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/register\"><i class=\"fa fa-user-plus\"></i> Sign up <span class=\"sr-only\">(current)</span></a>
                                <a class=\"nav-item nav-link\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/login\"><i class=\"fa fa-sign-in\"></i> Log in</a>
                            </div>
                        </div>
                    </nav>
                    
                </div>
            </header>

            <div>
                ";
        // line 34
        $this->displayBlock('main', $context, $blocks);
        // line 37
        echo "            </div>

            
            

        </section>

        <script src=\"";
        // line 44
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/libs/jquery/dist/jquery.min.js\"></script>
        <script src=\"";
        // line 45
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/libs/bootstrap/dist/js/bootstrap.min.js\"></script>
    </body>
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        echo "Wallet";
    }

    // line 34
    public function block_main($context, array $blocks = [])
    {
        // line 35
        echo "                ...
                ";
    }

    public function getTemplateName()
    {
        return "_global/login.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 35,  113 => 34,  107 => 5,  99 => 45,  95 => 44,  86 => 37,  84 => 34,  72 => 25,  68 => 24,  59 => 18,  50 => 12,  46 => 11,  42 => 10,  38 => 9,  33 => 6,  31 => 5,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_global/login.html", "C:\\xampp\\htdocs\\wallet\\views\\_global\\login.html");
    }
}
