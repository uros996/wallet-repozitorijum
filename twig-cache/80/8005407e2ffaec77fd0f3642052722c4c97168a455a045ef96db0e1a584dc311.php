<?php

/* UserCategoryManagement/getAdd.html */
class __TwigTemplate_3a155e7e16740f6e9996df70bb3fc37a3cf4dec3e68ba6361227bdbae449dbf4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "UserCategoryManagement/getAdd.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "<a href=\"";
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/categories\">
    List all categories
</a>

<form method=\"post\">
    <label for=\"name\">Ime kategorije: </label>
    <input type=\"text\" required name=\"name\" id=\"name\" placeholder=\"Unesite ime nove kategorije\"><br>

    <button type=\"submit\">Dodaj</button>
</form>
";
    }

    // line 16
    public function block_naslov($context, array $blocks = [])
    {
        // line 17
        echo "Dodavanje kategorije
";
    }

    public function getTemplateName()
    {
        return "UserCategoryManagement/getAdd.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 17,  52 => 16,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "UserCategoryManagement/getAdd.html", "C:\\xampp\\htdocs\\wallet\\views\\UserCategoryManagement\\getAdd.html");
    }
}
