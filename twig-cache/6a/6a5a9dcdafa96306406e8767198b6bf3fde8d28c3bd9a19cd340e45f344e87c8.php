<?php

/* Main/getRegister.html */
class __TwigTemplate_97ac7aa22f201a6461ae7b27f185df03ce9436e4e39f2931d5e7be42806dd618 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/login.html", "Main/getRegister.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/login.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "    <div class=\"col col-12 col-sm-12 col-md-8 offset-md-2 col-lg-8 offset-lg-2\">
        <div class=\"card\">
            <div class=\"card-header\">
                <i class=\"fa fa-pencil\"></i>
                Sign up
            </div>

            <div class=\"card-body\">
                <form action=\"";
        // line 12
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "/user/register\" method=\"POST\" onsubmit=\"return validateForm()\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                                <span class=\"input-group-text\">
                                    <i class=\"fa fa-user\"></i>
                                </span>
                            </div>

                            <input type=\"text\" id=\"input_username\" name=\"reg_username\"
                                class=\"form-control\"
                                required placeholder=\"Enter username.\"
                                pattern=\"[^\\s]{3,}\" title=\"Enter 3 visible charachters in a row.\">
                        </div>
                        <div class=\"alert alert-danger d-none\" id=\"username-error-message\"></div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                                <span class=\"input-group-text\">
                                    <i class=\"fa fa-at\"></i>
                                </span>
                            </div>
                            
                            <input type=\"email\" id=\"input_email\" name=\"reg_email\"
                                class=\"form-control\"
                                required placeholder=\"Enter email.\">
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"form-group col col-12 col-md-6\">
                            <div>
                                <label for=\"input_forename\">Firstname:</label>
                                <input type=\"text\" id=\"input_forename\" name=\"reg_forename\"
                                    class=\"form-control\"
                                    required placeholder=\"Enter your firstname.\"
                                    pattern=\"^[a-zA-Z ]{1,64}\$\" title=\"Enter from 1 to 64 alphanumeric characters or space.\">
                                    </div>
                            <div class=\"alert alert-danger d-none\" id=\"forename-error-message\"></div>
                        </div>
                        

                        <div class=\"form-group col col-12 col-md-6\">
                            <label for=\"input_surname\">Lastname:</label>
                            <input type=\"text\" id=\"input_surname\" name=\"reg_surname\"
                                class=\"form-control\"
                                required placeholder=\"Enter your lastname.\"
                                pattern=\"^[a-zA-Z ]{1,64}\$\" title=\"Enter from 1 to 64 alphanumeric characters or space.\">
                            <div class=\"alert alert-danger d-none\" id=\"surname-error-message\"></div>

                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"form-group col col-12 col-sm-12 col-md-6\">
                            <label for=\"input_password_1\">Password:</label>
                            <input type=\"password\" id=\"input_password_1\" name=\"reg_password_1\"
                                class=\"form-control\"
                                required placeholder=\"Enter password.\" 
                                pattern=\".{12,}\">
                            <div class=\"alert alert-danger d-none\" id=\"password_1-error-message\"></div>
                        </div>

                        <div class=\"form-group col col-12 col-sm-12 col-md-6\">
                            <label for=\"input_password_2\">Re-enter password:</label>
                            <input type=\"password\" id=\"input_password_2\" name=\"reg_password_2\"
                                class=\"form-control\"
                                required placeholder=\"Re-enter password.\"
                                pattern=\".{12,}\" title=\"Enter 12 or more characters.\">
                            <div class=\"alert alert-danger d-none\" id=\"password_2-error-message\"></div>
                        </div>
                    </div>

                    <button type=\"submit\" class=\"btn btn-primary col col-12 col-md-6 offset-md-6 col-lg-4 offset-lg-8\">
                        <i class=\"fa fa-user-plus\"></i> Sign up
                    </button>
                </form>
            </div>
        </div>
    </div>
    <script src=\"";
        // line 94
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/js/validators.js\"></script>

";
    }

    // line 98
    public function block_naslov($context, array $blocks = [])
    {
        // line 99
        echo "Registration
";
    }

    public function getTemplateName()
    {
        return "Main/getRegister.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 99,  138 => 98,  131 => 94,  46 => 12,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "Main/getRegister.html", "C:\\xampp\\htdocs\\wallet\\views\\Main\\getRegister.html");
    }
}
