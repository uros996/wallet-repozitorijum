<?php

/* UserCategoryManagement/getEdit.html */
class __TwigTemplate_0a1dc129b1008222c13ba8b03c1a67d82c2fe10b4286926ccfbd190d08e3fc09 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "UserCategoryManagement/getEdit.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "<a href=\"";
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/categories\">
    List all categories
</a>

<form method=\"post\">
    <label for=\"name\">Ime kategorije: </label>
    <input type=\"text\" required name=\"name\" id=\"name\"
           value=\"";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "name", []));
        echo "\"><br>

    <button type=\"submit\">Izmeni</button>
</form>
";
    }

    // line 17
    public function block_naslov($context, array $blocks = [])
    {
        // line 18
        echo "Izmena kategorije
";
    }

    public function getTemplateName()
    {
        return "UserCategoryManagement/getEdit.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 18,  56 => 17,  47 => 11,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "UserCategoryManagement/getEdit.html", "C:\\xampp\\htdocs\\wallet\\views\\UserCategoryManagement\\getEdit.html");
    }
}
