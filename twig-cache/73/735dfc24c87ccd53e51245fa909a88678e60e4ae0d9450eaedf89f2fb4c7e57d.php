<?php

/* UserCategoryManagement/postEdit.html */
class __TwigTemplate_0b03883cdf7aa449b1574cc5dd5a63899b8801c1917a18350c5e15988c3ce7bf extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "UserCategoryManagement/postEdit.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "<a href=\"";
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/categories\">
    List all categories
</a>

<p>";
        // line 8
        echo twig_escape_filter($this->env, ($context["message"] ?? null));
        echo "</p>

";
    }

    // line 12
    public function block_naslov($context, array $blocks = [])
    {
        // line 13
        echo "Izmena kategorije
";
    }

    public function getTemplateName()
    {
        return "UserCategoryManagement/postEdit.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 13,  51 => 12,  44 => 8,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "UserCategoryManagement/postEdit.html", "C:\\xampp\\htdocs\\wallet\\views\\UserCategoryManagement\\postEdit.html");
    }
}
