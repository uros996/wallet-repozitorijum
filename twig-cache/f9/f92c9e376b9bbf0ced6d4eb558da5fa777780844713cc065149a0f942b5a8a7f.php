<?php

/* UserCashbox/getEdit.html */
class __TwigTemplate_8c5f9d42b5c812f8fb64f6c813377c7783a8b045677fdd4f65fb718bc2a63b0a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "UserCashbox/getEdit.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "    <div class=\"col col-12 col-sm-12 col-md-8 offset-md-2 col-lg-8 offset-lg-2\">
        <div class=\"card\">
            <div class=\"card-header\">
                <i class=\"fa fa-pencil\"></i>
                Edit cashbox
            </div>

            <div class=\"card-body\">
                <form action=\"";
        // line 12
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/cashboxes/edit/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["cashbox"] ?? null), "cashbox_id", []), "html", null, true);
        echo "\" method=\"POST\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <input type=\"text\" id=\"input_cashbox_name\" name=\"cashbox_name\"
                                class=\"form-control\"
                                required placeholder=\"Enter cashbox name.\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["cashbox"] ?? null), "name", []));
        echo "\"
                                pattern=\".*[^\\s]{3,}.*\">
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <input type=\"text\" id=\"input_description\" name=\"cashbox_description\"
                                class=\"form-control\"
                                required placeholder=\"Enter cashbox description.\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["cashbox"] ?? null), "description", []));
        echo "\"
                                pattern=\".*[^\\s]{3,}.*\">
                        </div>
                    </div>

                    <button type=\"submit\" class=\"btn btn-primary\">
                        <i class=\"fa fa-edit\"></i> Save
                    </button>
                </form>
            </div>
        </div>
    </div>
";
    }

    // line 40
    public function block_naslov($context, array $blocks = [])
    {
        // line 41
        echo "Registracija
";
    }

    public function getTemplateName()
    {
        return "UserCashbox/getEdit.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 41,  85 => 40,  68 => 26,  56 => 17,  46 => 12,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "UserCashbox/getEdit.html", "C:\\xampp\\htdocs\\wallet\\views\\UserCashbox\\getEdit.html");
    }
}
