<?php

/* Main/loginGet.html */
class __TwigTemplate_bc1d58c60f4cce75b27f481319e380a7ff472937b7f6478925659b5ad941f162 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/login.html", "Main/loginGet.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/login.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "<div class=\"col col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 col-xl-6 offset-xl-3\">
    <div class=\"card\">
        <div class=\"card-header\">
            <i class=\"fa fa-pencil\"></i> 
            User log in
        </div>
        <div class=\"card-body\">
            <form method=\"post\">
                <div class=\"form-group\">
                    <div class=\"input-group\">
                        <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\">
                                <i class=\"fa fa-user\"></i>
                            </span>
                        </div>

                        <input type=\"text\" id=\"username\" name=\"username\"
                            class=\"form-control\"
                            required placeholder=\"Enter a username.\"
                            pattern=\"^[a-zA-Z0-9._-]{2,64}\$\"
                            titile=\"Enter more then 2 alpha numeric characters or '.' , '_', '-'\">
                    </div>
                </div>
                <div class=\"form-group\">
                    <input type=\"password\" id=\"password\" name=\"password\"
                        class=\"form-control\"
                        required placeholder= \"Enter a password.\"
                        pattern=\".{12,}\"
                        titile=\"Enter more then 12 characters\">

                </div>
                <button type=\"submit\" class=\"btn btn-primary col col-12 col-md-4 offset-md-8\">
                    <i class=\"fa fa-sign-in\"></i> Log in
                </button> 
                            
            </form>
        </div>
    </div>
</div>
";
    }

    // line 45
    public function block_naslov($context, array $blocks = [])
    {
        // line 46
        echo "Login
";
    }

    public function getTemplateName()
    {
        return "Main/loginGet.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 46,  79 => 45,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "Main/loginGet.html", "C:\\xampp\\htdocs\\wallet\\views\\Main\\loginGet.html");
    }
}
