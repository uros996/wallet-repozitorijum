<?php

/* Main/loginGet.html */
class __TwigTemplate_a99f582a4fdc681040d564c981d94c8446f3a8dfe7798272a1b63a6ed8f65023 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "Main/loginGet.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "    <div>
        <form method=\"post\">
            Username: <input type=\"text\" name=\"username\"><br>
            Password: <input type=\"password\" name=\"password\"><br>
            <button type=\"submit\">Log in</button>
        </form>
    </div>
";
    }

    // line 13
    public function block_naslov($context, array $blocks = [])
    {
        // line 14
        echo "Logovanje
";
    }

    public function getTemplateName()
    {
        return "Main/loginGet.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 14,  47 => 13,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "Main/loginGet.html", "C:\\xampp\\htdocs\\wallet\\views\\Main\\loginGet.html");
    }
}
