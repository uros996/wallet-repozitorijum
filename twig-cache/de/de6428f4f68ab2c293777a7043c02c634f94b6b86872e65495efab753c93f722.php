<?php

/* UserCashbox/getDetail.html */
class __TwigTemplate_2c6bad3aaf60561eb6aa485903c0ed39487ad350471436553139c865a2c0b7f4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "UserCashbox/getDetail.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "<input type=\"number\" id=\"cashboxId\" value=";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["cashbox"] ?? null), "cashbox_id", []), "html", null, true);
        echo " hidden>
    <div class=\"col col-12 col-sm-12 col-md-10 offset-md-1 col-lg-10 offset-lg-1\">
    <div class=\"text-right\">
        <a class=\"btn btn-primary\" role=\"button\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/cashboxes/edit/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["cashbox"] ?? null), "cashbox_id", []), "html", null, true);
        echo "\">
            <i class=\"fa fa-pencil\"></i> Edit
        </a>
    </div>
        <div class=\"text-center\">
            <h1>";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["cashbox"] ?? null), "name", []));
        echo "</h1>
            <h3>Saldo: ";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["cashbox"] ?? null), "saldo", []));
        echo "</h3>
        </div>
        <div>
            <p>";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["cashbox"] ?? null), "description", []));
        echo "</p>
        </div>

        <div class=\"card\">
            <div class=\"card-header\">
                <i class=\"fa fa-pencil\"></i>
                Add new transaction record
            </div>

            <div class=\"card-body\">
            <form>
                <div class=\"row\">
                    <div class=\"form-group col col-12 col-md-10 offset-md-1 col-lg-4 offset-lg-1\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                                <span class=\"input-group-text\">\$</span>
                            </div>
                            <input type=\"number\" min=\"0.01\" step=\"0.01\" id=\"input_record_value\" name=\"record_value\"
                                class=\"form-control\"
                                required placeholder=\"0.00\">
                            <div class=\"input-group-append\">
                                <span class=\"input-group-text\">";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["cashbox"] ?? null), "currency", []));
        echo "</span>                               
                            </div>
                        </div>
                    </div>

                    <div class=\"form-group col col-12 col-md-10 offset-md-1 col-lg-5 offset-lg-1\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                                <label class=\"input-group-text\" for=\"select_category\">Category:</label>
                            </div>
                            <select class=\"custom-select\" id=\"select_category\" name=\"record_category\">
                                ";
        // line 48
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 49
            echo "                                    <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "category_id", []), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", []), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "category_type", []), "html", null, true);
            echo "</option>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "                            </select>
                        </div>
                    </div>
                </div>
            </form>
            <div class=\"col col-12 col-md-10 offset-md-1 text-right\"> 
            <button onclick=\"addRecord(";
        // line 57
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["cashbox"] ?? null), "cashbox_id", []), "html", null, true);
        echo ")\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i> Add</button>
            </div>
        </div>        
    </div>
    <br>
</div>
<div class=\"row justify-content-center\">
    <div class=\"col col-12 col-md-6 col-lg-3\">
        <canvas id=\"IncomePieChart\" width=\"250\" height=\"250\"></canvas>
    </div>
    <div class=\"col col-12 col-md-6 col-lg-3\">
        <canvas id=\"IncomeBarChart\" width=\"250\" height=\"250\"></canvas>
    </div>    
    <div class=\"col col-12 col-md-6 col-lg-3\">
        <canvas id=\"OutcomePieChart\" width=\"250\" height=\"250\"></canvas>
    </div>
    <div class=\"col col-12 col-md-6 col-lg-3\">
        <canvas id=\"OutcomeBarChart\" width=\"250\" height=\"250\"></canvas>
    </div>    
</div>
    <br>
<div class=\"row justify-content-center\">
    <div class=\"form-group col col-12 col-md-6 col-lg-3\">
        <div class=\"input-group\">
            <div class=\"input-group-prepend\">
                <label class=\"input-group-text\" for=\"input_daterecor_begin\">From:</label>
            </div>
            <input type=\"date\" id=\"input_daterecor_begin\" name=\"date_record_begin\" class=\"form-control\" value=\"";
        // line 84
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["cashbox"] ?? null), "created_at", []), "Y-m-d"), "html", null, true);
        echo "\" onchange=\"getTimeData()\">
        </div>
    </div>

    <div class=\"form-group col col-12 col-md-6 col-lg-4\">
        <div class=\"input-group\">
            <div class=\"input-group-prepend\">
                <label class=\"input-group-text\" for=\"input_daterecor_end\">To: </label>
            </div>
            <input type=\"date\" id=\"input_daterecor_end\" name=\"date_record_end\" class=\"form-control\" onchange=\"getTimeData()\">
        </div>
    </div>
</div>

<div class=\"row justify-content-center\">
    <div class=\"col col-12 col-md-6 col-lg-4\">
        <canvas id=\"RecordThrowTime\" width=\"300\" height=\"350\"></canvas>
    </div>   
</div> 
    
    
    <div class=\"table-responsive\">
        <table class=\"table\">
        <thead>
            <tr>
                <th scope=\"col\"></th>
                <th scope=\"col\">Category</th>
                <th scope=\"col\">Type</th>
                <th scope=\"col\">Created at</th>
                <th scope=\"col\">Cash</th>
            </tr>
        </thead>
        <tbody id=\"tbody_id\">
            ";
        // line 117
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 118
            echo "                <tr>
                <th scope=\"row\"></th>
                <td>";
            // line 120
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "name", []), "html", null, true);
            echo "</td>
                <td>";
            // line 121
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "category_type", []), "html", null, true);
            echo "</td>
                <td>";
            // line 122
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "created_at", []), "html", null, true);
            echo "</td>
                <td>";
            // line 123
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "cash", []), "html", null, true);
            echo "</td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 126
        echo "        </tbody>
        </table>
        
    </div>
    
    <script src=\"";
        // line 131
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/js/records.js\"></script>
    
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js\"></script>

  
";
    }

    // line 139
    public function block_naslov($context, array $blocks = [])
    {
        // line 140
        echo "Cashbox detail
";
    }

    public function getTemplateName()
    {
        return "UserCashbox/getDetail.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  242 => 140,  239 => 139,  228 => 131,  221 => 126,  212 => 123,  208 => 122,  204 => 121,  200 => 120,  196 => 118,  192 => 117,  156 => 84,  126 => 57,  118 => 51,  105 => 49,  101 => 48,  87 => 37,  63 => 16,  57 => 13,  53 => 12,  43 => 7,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "UserCashbox/getDetail.html", "C:\\xampp\\htdocs\\wallet\\views\\UserCashbox\\getDetail.html");
    }
}
