<?php

/* Main/getRegister.html */
class __TwigTemplate_72d117329c3ba53f7f8da2d1fb97632a26107036bc20610ad3e179e61fb8c4ba extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "Main/getRegister.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "    <div class=\"col col-12 col-md-6 offset-md-3\">
        <div class=\"card\">
            <div class=\"card-header\">
                <i class=\"fa fa-pencil\"></i>
                Registracija naloga
            </div>

            <div class=\"card-body\">
                <form method=\"post\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                                <span class=\"input-group-text\">
                                    <i class=\"fa fa-user\"></i>
                                </span>
                            </div>

                            <input type=\"text\" id=\"input_username\" name=\"reg_username\"
                                class=\"form-control\"
                                required placeholder=\"Unesite željeno korisničko ime.\">
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                                <span class=\"input-group-text\">
                                    <i class=\"fa fa-at\"></i>
                                </span>
                            </div>
                            
                            <input type=\"email\" id=\"input_email\" name=\"reg_email\"
                                class=\"form-control\"
                                required placeholder=\"Unesite adresu svoje e-pošte.\">
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"form-group col col-12 col-md-6\">
                            <label for=\"input_forename\">Ime:</label>
                            <input type=\"text\" id=\"input_forename\" name=\"reg_forename\"
                                class=\"form-control\"
                                required placeholder=\"Unesite svoje ime.\">
                        </div>

                        <div class=\"form-group col col-12 col-md-6\">
                            <label for=\"input_surname\">Prezime:</label>
                            <input type=\"text\" id=\"input_surname\" name=\"reg_surname\"
                                class=\"form-control\"
                                required placeholder=\"Unesite svoje prezime.\">
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"form-group col col-12 col-md-6\">
                            <label for=\"input_password_1\">Lozinka:</label>
                            <input type=\"password\" id=\"input_password_1\" name=\"reg_password_1\"
                                class=\"form-control\"
                                required placeholder=\"Unesite željenu lozinku.\">
                        </div>

                        <div class=\"form-group col col-12 col-md-6\">
                            <label for=\"input_password_2\">Potvrda lozinke:</label>
                            <input type=\"password\" id=\"input_password_2\" name=\"reg_password_2\"
                                class=\"form-control\"
                                required placeholder=\"Ponovite željenu lozinku.\">
                        </div>
                    </div>

                    <button type=\"submit\" class=\"btn btn-primary col col-12 col-md-4\">
                        <i class=\"fa fa-user-plus\"></i> Registracija
                    </button>
                </form>
            </div>
        </div>
    </div>
";
    }

    // line 82
    public function block_naslov($context, array $blocks = [])
    {
        // line 83
        echo "Registracija
";
    }

    public function getTemplateName()
    {
        return "Main/getRegister.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 83,  116 => 82,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "Main/getRegister.html", "C:\\xampp\\htdocs\\wallet\\views\\Main\\getRegister.html");
    }
}
