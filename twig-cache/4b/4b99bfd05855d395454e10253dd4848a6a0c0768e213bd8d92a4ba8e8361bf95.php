<?php

/* UserCashbox/cashboxes.html */
class __TwigTemplate_06031d857cd1c60db34a5599fff0a19ad4a738dfd0b983c2441880db572bdad1 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "UserCashbox/cashboxes.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "
    <a class=\"btn btn-primary\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/cashboxes/add\" role=\"button\">
        <i class=\"fa fa-plus\"></i> Add a new cashbox
    </a>
    <br>
    
<div class=\"d-flex flex-column-reverse my-flex-container-column align-items-center\">
    ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cashboxes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["cashbox"]) {
            // line 12
            echo "        <div class=\"col col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8\" id=\"cashbox-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cashbox"], "cashbox_id", []), "html", null, true);
            echo "\">
            <div class=\"p-2 my-flex-item d-none\" id=\"cashboxId_";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cashbox"], "cashbox_id", []), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cashbox"], "priority", []));
            echo "</div>
            <div class=\"card\">
                <div class=\"card-header\">
                    <div class=\"row\">
                        <div class=\"col col-12 col-md-6 col-lg-8\">
                        <p class=\"h2\"> 
                            <a href=\"";
            // line 19
            echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
            echo "user/cashboxes/detail/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cashbox"], "cashbox_id", []), "html", null, true);
            echo "\" class=\"col col-10 card-link\">
                                ";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cashbox"], "name", []));
            echo "
                            </a>
                        </p>
                        </div>
                        <div class=\"col col-12 col-md-6 col-lg-4 text-right\">
                            <span class=\"card-text text-secondary\">Priority:</span>
                        
                            <button type=\"button\" class=\"btn btn-link text-danger\" onclick=\"decrementPriority(";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cashbox"], "cashbox_id", []), "html", null, true);
            echo ")\">
                                    <i class=\"fa fa-minus\"></i>
                                                                    
                            </button>
                            <button type=\"button\" class=\"btn btn-link text-success\" onclick=\"incrementPriority(";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cashbox"], "cashbox_id", []), "html", null, true);
            echo ")\">
                                    <i class=\"fa fa-plus\"></i>
                                                                    
                            </button>
                        </div>
                    </div>
                </div>
                <div class=\"card-body\">
                    <p  class=\"card-text\">
                        ";
            // line 40
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cashbox"], "description", []));
            echo "
                    </p>
                    <p  class=\"card-text text-secondary\">
                    
                        <b>Last change</b>
                        <br>
                        ";
            // line 46
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cashbox"], "record_created_at", []), "d-m-Y H:m"), "html", null, true);
            echo "
                        <br>
                        ";
            // line 48
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cashbox"], "category_type", []));
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cashbox"], "cash", []));
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cashbox"], "currency", []));
            echo "
                    </p>                   
                    <h2 class=\"card-text text-right\"> ";
            // line 50
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cashbox"], "saldo", []));
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cashbox"], "currency", []));
            echo "</h2>
                </div>
            </div>
        <br>
        </div>
        
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cashbox'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "</div>
<script src=\"";
        // line 58
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/js/cashboxes.js\"></script>
";
    }

    // line 61
    public function block_naslov($context, array $blocks = [])
    {
        // line 62
        echo "Cashbox list
";
    }

    public function getTemplateName()
    {
        return "UserCashbox/cashboxes.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 62,  149 => 61,  143 => 58,  140 => 57,  125 => 50,  117 => 48,  112 => 46,  103 => 40,  91 => 31,  84 => 27,  74 => 20,  68 => 19,  57 => 13,  52 => 12,  48 => 11,  39 => 5,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "UserCashbox/cashboxes.html", "C:\\xampp\\htdocs\\wallet\\views\\UserCashbox\\cashboxes.html");
    }
}
