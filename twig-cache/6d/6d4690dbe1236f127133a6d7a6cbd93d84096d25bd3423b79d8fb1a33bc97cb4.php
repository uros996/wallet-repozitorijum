<?php

/* UserCategoryManagement/getEdit.html */
class __TwigTemplate_20fb2df926a05cbea30455d998c0c0ef5f948a1a5f6005038fd01989500d8aeb extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "UserCategoryManagement/getEdit.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "    <div class=\"card\">
            <div class=\"card-header\">
                <i class=\"fa fa-pencil\"></i>
                Edit category
            </div>

            <div class=\"card-body\">
                <form action=\"";
        // line 11
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/categories/edit/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "category_id", []), "html", null, true);
        echo "\" method=\"POST\">
                <div class=\"row\">
                    <div class=\"form-group col col-12 col-md-6\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                                <span class=\"input-group-text\">Name</span>
                            </div>
                            <input type=\"text\" id=\"name\" name=\"name\"
                                class=\"form-control\"
                                value=\"";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "name", []));
        echo "\"
                                required placeholder=\"Enter category name\"
                                pattern=\".*[^\\s]{3,}.*\">
                        </div>
                    </div>
                    <div class=\"form-group col col-12 col-md-4\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                                <label class=\"input-group-text\" for=\"select_type\">Type:</label>
                            </div>
                            <input type=\"text\" id=\"select_type\" name=\"type\"
                                class=\"form-control\"
                                value=\"";
        // line 32
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "category_type", []));
        echo "\"
                                disabled>
                        </div>
                    </div>
                    <div class=\"col col-12 col-md-2\"> 
                        <button type=\"submit\" class=\"btn btn-primary pull-right\">
                            <i class=\"fa fa-plus\"></i>
                        </button>
                    </div>
                </div>
                </form>
                
            </div>
        </div>
";
    }

    // line 48
    public function block_naslov($context, array $blocks = [])
    {
        // line 49
        echo "Izmena kategorije
";
    }

    public function getTemplateName()
    {
        return "UserCategoryManagement/getEdit.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 49,  93 => 48,  74 => 32,  59 => 20,  45 => 11,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "UserCategoryManagement/getEdit.html", "C:\\xampp\\htdocs\\wallet\\views\\UserCategoryManagement\\getEdit.html");
    }
}
