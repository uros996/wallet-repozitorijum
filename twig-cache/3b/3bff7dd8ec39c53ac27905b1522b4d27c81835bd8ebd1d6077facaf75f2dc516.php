<?php

/* UserCashbox/getAdd.html */
class __TwigTemplate_5780a72f90eabe35920a02557b9c834fe4e020d75cdf4f7f73447396cc52dd67 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "UserCashbox/getAdd.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "    <div class=\"col col-12 col-sm-12 col-md-8 offset-md-2 col-lg-8 offset-lg-2\">
        <div class=\"card\">
            <div class=\"card-header\">
                <i class=\"fa fa-pencil\"></i>
                New Cashbox
            </div>

            <div class=\"card-body\">
                <form action=\"";
        // line 12
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/cashboxes/add\" method=\"POST\" onsubmit=\"return validateCashboxForm()\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                                <label class=\"input-group-text\" for=\"input_cashbox_name\">Name:</label>
                            </div>                            
                            <input type=\"text\" id=\"input_cashbox_name\" name=\"cashbox_name\"
                                class=\"form-control\"
                                required placeholder=\"Cashbox name.\"
                                pattern=\".*[^\\s]{3,}.*\"
                                title=\"Enter 3 or more visible characters.\">
                            </div>
                        <div class=\"alert alert-danger d-none\" id=\"cashbox_name-error-message\"></div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <input type=\"text\" id=\"input_description\" name=\"cashbox_description\"
                                class=\"form-control\"
                                required placeholder=\"Cashbox description.\"
                                pattern=\".*[^\\s]{3,}.*\"
                                title=\"Enter 3 or more visible characters.\">
                        </div>
                        <div class=\"alert alert-danger d-none\" id=\"cashbox_description-error-message\"></div>
                    </div>

                    
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                                <label class=\"input-group-text\" for=\"select_currency\">Currency:</label>
                            </div>
                            <select class=\"custom-select\" id=\"select_currency\" name=\"cashbox_currency\" >
                                <option value=\"RSD\">RSD</option>
                                <option value=\"EUR\">EUR</option>
                                <option value=\"USD\">USD</option>
                            </select>
                        </div>
                    </div>
                    <button type=\"submit\" class=\"btn btn-primary\">
                        <i class=\"fa fa-plus\"></i> Cashbox
                    </button>
                </form>
            </div>
        </div>
    </div>
    <script src=\"";
        // line 58
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/js/validators.js\"></script>

";
    }

    // line 62
    public function block_naslov($context, array $blocks = [])
    {
        // line 63
        echo "Add cashbox
";
    }

    public function getTemplateName()
    {
        return "UserCashbox/getAdd.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 63,  102 => 62,  95 => 58,  46 => 12,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "UserCashbox/getAdd.html", "C:\\xampp\\htdocs\\wallet\\views\\UserCashbox\\getAdd.html");
    }
}
