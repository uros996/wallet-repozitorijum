<?php

/* UserCategoryManagement/categories.html */
class __TwigTemplate_ec0854031af6d70c63ae1538d8411ffc2b5d49847eb02854b5b9949604e5fec0 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "UserCategoryManagement/categories.html", 1);
        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        // line 4
        echo "

<div>
    <a class=\"btn btn-primary\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/categories/add\" role=\"button\">
        <i class=\"fa fa-plus\"></i> Add a new category
    </a>
    <div class=\"col col-12 col-md-8 col-lg-6 offset-lg-3 col-xl-6 offset-xl-3\">
     <div class=\"table-responsive\">
        <table class=\"table\">
            <thead class=\"thead-dark\">
            <tr class=\"text-center\">
                <th> Categories </th>
                <th></th>
            </tr>
            
            </thead>
            <tbody>  
                ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 22
            echo "                <tr>
                    <td>
                        ";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", []));
            echo "
                    </td>
                    <td class=\"text-center\">
                        <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
            echo "user/categories/edit/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "category_id", []), "html", null, true);
            echo "\">
                            <i class=\"fa fa-pencil\"></i>
                        </a>
                    </td>
                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "            </tbody>
        </table>
    </div>
    </div>
   
            <a href=\"";
        // line 38
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/categories/edit/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "category_id", []), "html", null, true);
        echo "\">
                ";
        // line 39
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "name", []));
        echo "
            </a>
   
</div>
";
    }

    // line 45
    public function block_naslov($context, array $blocks = [])
    {
        // line 46
        echo "Categories list
";
    }

    public function getTemplateName()
    {
        return "UserCategoryManagement/categories.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 46,  108 => 45,  99 => 39,  93 => 38,  86 => 33,  72 => 27,  66 => 24,  62 => 22,  58 => 21,  41 => 7,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "UserCategoryManagement/categories.html", "C:\\xampp\\htdocs\\wallet\\views\\UserCategoryManagement\\categories.html");
    }
}
