<?php

/* Main/showCategoryAuctions.html */
class __TwigTemplate_4129fa3792b76a7d71087408453706bf9c50d7ed15c7c7e59759f89b85b2470b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "Main/showCategoryAuctions.html", 1);
        $this->blocks = [
            'naslov' => [$this, 'block_naslov'],
            'title' => [$this, 'block_title'],
            'main' => [$this, 'block_main'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_naslov($context, array $blocks = [])
    {
        // line 4
        echo "Aukcije iz kategorije ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "name", []));
        echo "
";
    }

    // line 7
    public function block_title($context, array $blocks = [])
    {
        // line 8
        echo "Kategorija \"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "name", []));
        echo "\"
";
    }

    // line 11
    public function block_main($context, array $blocks = [])
    {
        // line 12
        echo "<ul class=\"auction-list\">
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["auctions"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["auction"]) {
            // line 14
            echo "    <li class=\"auction-list-item\">
        <a href=\"";
            // line 15
            echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
            echo "auction/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["auction"], "auction_id", []), "html", null, true);
            echo "\">
            ";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["auction"], "title", []));
            echo ",
            ";
            // line 17
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["auction"], "ends_at", []), "j. n. Y. H.i"), "html", null, true);
            echo "
        </a>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['auction'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "</ul>

";
        // line 22
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "2019-03-12T14:21:05+00:00", "d.m.Y. H:i:s", "Asia/Tokyo"), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "Main/showCategoryAuctions.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 22,  86 => 20,  77 => 17,  73 => 16,  67 => 15,  64 => 14,  60 => 13,  57 => 12,  54 => 11,  47 => 8,  44 => 7,  37 => 4,  34 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "Main/showCategoryAuctions.html", "C:\\xampp\\htdocs\\wallet\\views\\Main\\showCategoryAuctions.html");
    }
}
