<?php
    use App\Core\Route;

    return [
        Route::get('#^user/register/?$#',                  'Main',                  'getRegister'),
        Route::post('#^user/register/?$#',                 'Main',                  'postRegister'),
        Route::get('#^user/login/?$#',                     'Main',                  'loginGet'),
        Route::post('#^user/login/?$#',                    'Main',                  'loginPost'),
        Route::get('#^user/logout/?$#',                    'Main',                  'logoutGet'),
       
        
    
        Route::get('#^api/categories/?$#',                'MainApi',                'categories'),

        Route::get('#^api/priority/?$#',                  'CashboxApi',             'getPriorities'),
        Route::get('#^api/priority/increment/([0-9]+)?$#','CashboxApi',             'getIncrementPriority'),
        Route::get('#^api/priority/decrement/([0-9]+)?$#','CashboxApi',             'getDecrementPriority'),

        Route::get('#^api/records/([0-9]+)/?$#',          'RecordApi',              'getRecords'),
        Route::post('#^api/records/add/([0-9]+)/?$#',     'RecordApi',              'postAdd'),
        Route::get('#^api/records/income/([0-9]+)/?$#',   'RecordApi',              'getIncome'),
        Route::get('#^api/records/outcome/([0-9]+)/?$#',  'RecordApi',              'getOutcome'),
        Route::post('#^api/records/interval/?$#',         'RecordApi',              'getIntervalRecords'),

        Route::get('#^user/cashboxes/?$#',                 'UserCashbox',           'cashboxes'),
        Route::get('#^user/cashboxes/add/?$#',             'UserCashbox',           'getAdd'),
        Route::post('#^user/cashboxes/add/?$#',            'UserCashbox',           'postAdd'),
        Route::get('#^user/cashboxes/detail/([0-9]+)/?$#', 'UserCashbox',           'getDetail'),
        Route::get('#^user/cashboxes/edit/([0-9]+)/?$#',   'UserCashbox',           'getEdit'),
        Route::post('#^user/cashboxes/edit/([0-9]+)/?$#',  'UserCashbox',           'postEdit'),

        Route::get('#^user/categories/?$#',                'UserCategoryManagement', 'categories'),
        Route::get('#^user/categories/add/?$#',            'UserCategoryManagement', 'getAdd'),
        Route::post('#^user/categories/add/?$#',           'UserCategoryManagement', 'postAdd'),
        Route::get('#^user/categories/edit/([0-9]+)/?$#',  'UserCategoryManagement', 'getEdit'),
        Route::post('#^user/categories/edit/([0-9]+)/?$#', 'UserCategoryManagement', 'postEdit'),

        # Fallback
        Route::get('#^.*$#', 'UserCashbox', 'cashboxes')
    ];
