function addRecord(id) {
    var form = new FormData();
    let cashValue = document.getElementById('input_record_value'); 
    let categoryId = document.getElementById('select_category');
    
    form.append('record_value', cashValue.value);
    form.append('record_category', categoryId.value);
    form.append('cashbox_id', id);
    
    fetch('/wallet/api/records/add/' + id, {
        method: 'POST',
        credentials: 'include',
        body:form
    })
    .then(response => response.json())
    .then(json => {
        
        if (json.new_record != undefined){ 
        let data = json.new_record[0];
        addRow(data);
        getIncomeChartsData();
        getOutcomeChartsData();
        getTimeData();
        cashValue.value = 0.01;
        alert("Saldo has changed!")
        } else {
            alert("Not enoguh money in current saldo")
        }
    })
    .catch(error => console.log('Error:', error));
}

function addRow(newRecord){
    
    let tableBody = document.getElementById('tbody_id');

    tableBody.append
    let tr = document.createElement('tr');
    let th = document.createElement('th');
    let td1 = document.createElement('td');
    let td2 = document.createElement('td');
    let td3 = document.createElement('td');
    let td4 = document.createElement('td');
    
    th.setAttribute('scope', 'row');
    td1.innerText= newRecord.name;
    td2.innerText= newRecord.category_type;
    td3.innerText= newRecord.created_at;
    td4.innerText= newRecord.cash;

    tr.appendChild(th);
    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);

    tableBody.prepend(tr);
}



function getRecords() {
    id = document.getElementById("cashboxId").value;
    fetch('/wallet/api/records/' + id, {
        credentials: "include"
    })
    .then(response => response.json())
    .then(json => {
        let records = json.records;
        
        // displayRecords(records);
    });
}

function getIncomeChartsData() {
    id = document.getElementById("cashboxId").value;
    fetch('/wallet/api/records/income/' + id, {
            credentials: "include"
        })
        .then(response => response.json())
        .then(json => {
            
            let parcentage = json.parcentage;
            displayCharts(parcentage, 'IncomePieChart', 'IncomeBarChart');
            
        });
}

function getOutcomeChartsData() {
    id = document.getElementById("cashboxId").value;
    fetch('/wallet/api/records/outcome/' + id, {
            credentials: "include"
        })
        .then(response => response.json())
        .then(json => {
            
            let parcentage = json.parcentage;
            displayCharts(parcentage, 'OutcomePieChart', 'OutcomeBarChart');    
        });
}

function displayCharts(parcentage, pie_id, bar_id) {
    

    var ctx = document.getElementById(pie_id);
    var ctx1 = document.getElementById(bar_id);
    
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: parcentage.map(value =>value.name),
            datasets: [{
                
                data: parcentage.map(value =>value.cash),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                display: false
            },
            responsive:true,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    var myChart1 = new Chart(ctx1, {
        type: 'bar',
        data: {
            labels: parcentage.map(value =>value.name),
            datasets: [{
                label:'',
                data: parcentage.map(value =>value.cash),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            legend: {
            display: false
        },
            responsive:true,
            scales: {
                xAxes: [{
                    ticks: {
                        display:false,
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        
                    }
                }]
            }
        }
        
    });
}

function getTimeData(){
    
    var form = new FormData();
    let cashboxId = document.getElementById('cashboxId'); 
    let recordCreatedFrom = document.getElementById('input_daterecor_begin');
    let recordCreatedTo = document.getElementById('input_daterecor_end');
    from = recordCreatedFrom.value
    to = recordCreatedTo.value
    
    
    

    if (to.length==0){
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10){
                dd='0'+dd
            } 
            if(mm<10){
                mm='0'+mm
            } 

        today =yyyy+'-'+mm+'-'+dd;
        var dateTime = today;
        to = dateTime;
        recordCreatedTo.value = to;
    }
    
    a  = new Date(from) >= new Date(to)
    d = new Date(from)
    
    if (new Date(from) > new Date(to)){
        alert("From time cannot be higher than to");
        from = to;
        recordCreatedFrom.value = from
    }
    
    form.append('cashbox_id', cashboxId.value);
    form.append('record_created_from', from);
    form.append('record_created_to', to);
    
    fetch('/wallet/api/records/interval/', {
        method: 'POST',
        credentials: 'include',
        body:form
    })
    .then(response =>response.json())
    .then(json => {
        let data = json.records_interval;
        if(data.length > 0) {
            displayTimeCharts(data);
        } else {
            displayNoDataChart();
        }    
        
    })
    .catch(error => console.log('Error:', error));
}

function displayTimeCharts(nesto) {
    

    var ctx3 = document.getElementById("RecordThrowTime");
    
    var myChart = new Chart(ctx3, {
        
        type: 'line',
        data: {
            labels: nesto.map(value =>value.created_at),
            datasets: [{
                
                data: nesto.map(value =>value.last_saldo),
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                borderColor: 'rgba(255, 99, 132, 1)',
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                display: false
            },
            responsive:true,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

}

function displayNoDataChart() {
    

    var c = document.getElementById("RecordThrowTime");
    var ctx3 = c.getContext("2d");
    ctx3.clearRect(0, 0, c.width, c.height);
    ctx3.font = "19px Arial";
    ctx3.fillStyle = "#ff0000"
    ctx3.fillText("Ther is no data for this time period!", 0, 100);
}
window.addEventListener('load', getIncomeChartsData);
window.addEventListener('load', getOutcomeChartsData);
window.addEventListener('load', getTimeData);
window.addEventListener('load', getRecords);