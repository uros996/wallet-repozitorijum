function validateRegisterForm() {
    let status = true;
    const username = document.querySelector('#input_username').value;
    const forename = document.querySelector('#input_forename').value;
    const surname  = document.querySelector('#input_surname').value;
    const password_1  = document.querySelector('#input_password_1').value;
    const password_2  = document.querySelector('#input_password_2').value;
    
    document.querySelector('#username-error-message').classList.add('d-none');
    document.querySelector('#forename-error-message').classList.add('d-none');
    document.querySelector('#surname-error-message').classList.add('d-none');
    document.querySelector('#password_1-error-message').classList.add('d-none');
    document.querySelector('#password_2-error-message').classList.add('d-none');

    if (!username.match(/.*[^\s]{3,}.*/)){
        document.querySelector('#username-error-message').innerHTML = "The username must contain at least 3 visible characters.";
        document.querySelector('#username-error-message').classList.remove('d-none');

        status = false;
    }

    if (!forename.match(/.*[^\s]{2,}.*/)){
        document.querySelector('#forename-error-message').innerHTML = "The firstname must contain at least 2 visible characters.";
        document.querySelector('#forename-error-message').classList.remove('d-none');

        status = false;
    }

    if (!surname.match(/.*[^\s]{2,}.*/)){
        document.querySelector('#surname-error-message').innerHTML = "The lastname must contain at least 2 visible characters.";
        document.querySelector('#surname-error-message').classList.remove('d-none');

        status = false;
    }

    if (password_1.length < 12) {
        document.querySelector('#password_1-error-message').innerHTML = "The password must contain at least 12 characters.";
        document.querySelector('#password_1-error-message').classList.remove('d-none');

        status = false;
    }

    if (password_2.length < 12){
        document.querySelector('#password_2-error-message').innerHTML = "The password must contain at least 12 characters.";
        document.querySelector('#password_2-error-message').classList.remove('d-none');

        status = false;
    }

    

    return status;
}

function validateLoginForm() {
    let status = true;
    const username = document.querySelector('#input_username').value;
    const password  = document.querySelector('#password').value;

    document.querySelector('#username-error-message').classList.add('d-none');
    document.querySelector('#password-error-message').classList.add('d-none');

    if (!username.match(/.*[^\s]{3,}.*/)){
        document.querySelector('#username-error-message').innerHTML = "The username must contain at least 3 visible characters.";
        document.querySelector('#username-error-message').classList.remove('d-none');

        status = false;
    }

    if (password.length < 12) {
        document.querySelector('#password-error-message').innerHTML = "The password must contain at least 12 characters.";
        document.querySelector('#password-error-message').classList.remove('d-none');

        status = false;
    }

    return status;
}

function validateCashboxForm() {
    let status = true;
    const cashboxName = document.querySelector('#input_cashbox_name').value;
    const description  = document.querySelector('#input_description').value;

    document.querySelector('#cashbox_name-error-message').classList.add('d-none');
    document.querySelector('#cashbox_description-error-message').classList.add('d-none');

    if (!cashboxName.match(/.*[^\s]{3,}.*/)){
        document.querySelector('#cashbox_name-error-message').innerHTML = "The cashbox name must contain at least 3 visible characters.";
        document.querySelector('#cashbox_name-error-message').classList.remove('d-none');

        status = false;
    }

    if (!description.match(/.*[^\s]{3,}.*/)) {
        document.querySelector('#cashbox_description-error-message').innerHTML = "The description must contain at least 3 visible characters.";
        document.querySelector('#cashbox_description-error-message').classList.remove('d-none');

        status = false;
    }

    return status;
}

function validateCategoryForm() {
    let status = true;
    const categoryName = document.querySelector('#name').value;
    
    document.querySelector('#name-error-message').classList.add('d-none');
    
    if (!cashboxName.match(/.*[^\s]{3,}.*/)){
        document.querySelector('#name-error-message').innerHTML = "The category name must contain at least 3 visible characters.";
        document.querySelector('#name-error-message').classList.remove('d-none');

        status = false;
    }

    return status;
}
