function getPriority() {
    fetch('/wallet/api/priority/', {
        credentials: "include"
    })
    .then(response => response.json())
    .then(json => {
        
        let neko = json.priorities;

        for ( let cb of neko ) {
            let cbi = document.getElementById('cashbox-' + cb.cashbox_id);
            cbi.style.order = cb.priority;
        }
        
        // displayRecords(records);
    });
}

function incrementPriority(id){
    fetch('/wallet/api/priority/increment/' + id, {
        credentials: "include"
    })
    .then(response => response.json())
    .then(json => {
        
        let neko = json.priority;

        let cbi = document.getElementById('cashbox-' + id);
        cbi.style.order = neko;

        console.log(json)
        // displayRecords(records);
    });

}

function decrementPriority(id){
    fetch('/wallet/api/priority/decrement/' + id, {
        credentials: "include"
    })
    .then(response => response.json())
    .then(json => {
        
        let neko = json.priority;

        
        let cbi = document.getElementById('cashbox-' + id);
        cbi.style.order = neko;

        console.log(neko)
        // displayRecords(records);
    });
    

}


window.addEventListener('load', getPriority);