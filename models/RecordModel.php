<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use \PDO;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class RecordModel extends Model {
        protected function getFields() {
            return [
                'record_id'     => new Field(
                                        (new NumberValidator())
                                            ->setInteger()
                                            ->setUnsigned()
                                            ->setMaxIntegerDigits(11), false),
                'created_at'     => new Field(new DateTimeValidator(), false),
                
                'cash' => new Field(
                                (new NumberValidator())
                                        ->setUnsigned()
                                        ->setMaxDecimalDigits(2)),
                                        
                'category_id'    => new Field(
                                        (new NumberValidator())
                                            ->setInteger()
                                            ->setUnsigned()
                                            ->setMaxIntegerDigits(11)),
                'cashbox_id'        => new Field(
                                        (new NumberValidator())
                                            ->setInteger()
                                            ->setUnsigned()
                                            ->setMaxIntegerDigits(11)),
                
            ];
        }

        public function getAllByCategoryId(int $categoryId): array {
            return $this->getAllByFieldName('category_id', $categoryId);
        }

        // public function getRecordByRecordId(int $recordId) {
        //     return $this->getByFieldName('record_id', $recordId);
        // }

        public function getAllByCashboxId(int $cashboxId): array {
            $pdo = $this->getDatabaseConnection()->getConnection();
            $sql = 'SELECT `record`.`cash`,`record`.`created_at`,`category`.`name`, `category`.`category_type` FROM record LEFT JOIN category ON `category`.`category_id` = `record`.`category_id` WHERE cashbox_id=?  ORDER BY created_at DESC;';
            $prep = $pdo->prepare($sql);
            $items = [];

            if ($prep) {
                $res = $prep->execute([ $cashboxId ]);

                if ($res) {
                    $items = $prep->fetchAll(PDO::FETCH_OBJ);
                }
            }

            return $items;
        }
        public function getRecordByRecordId(int $recordId) {
            $pdo = $this->getDatabaseConnection()->getConnection();
            $sql = 'SELECT `record`.`cash`,`record`.`created_at`,`category`.`name`, `category`.`category_type` FROM record LEFT JOIN category ON `category`.`category_id` = `record`.`category_id` WHERE record_id=?;';
            $prep = $pdo->prepare($sql);
            $items;

            if ($prep) {
                $res = $prep->execute([ $recordId ]);

                if ($res) {
                    $items = $prep->fetchAll(PDO::FETCH_OBJ);
                }
            }

            return $items;
        }
        public function getSumByCashboxId(int $cashboxId, int $userId, $type) {
            $pdo = $this->getDatabaseConnection()->getConnection();
            $sql = 'SELECT SUM(record.cash) as cash, `record`.`category_id`, `category`.`name` FROM record 
                LEFT JOIN category ON `category`.`category_id` = `record`.`category_id` 
                WHERE record.cashbox_id=? AND category.category_type=? AND category.user_id=? GROUP BY `record`.`category_id`;';
            $prep = $pdo->prepare($sql);
            $items;

            if ($prep) {
                $res = $prep->execute([ $cashboxId, $type, $userId  ]);

                if ($res) {
                    $items = $prep->fetchAll(PDO::FETCH_OBJ);
                }
            }

            return $items;
        }

        public function getSumByCategoryId(int $categoryId, int $cashboxId): array {
            $pdo = $this->getDatabaseConnection()->getConnection();
            $sql = 'SELECT COALESCE(SUM(cash),0) as sum FROM record WHERE category_id=? AND cashbox_id=?;';
            $prep = $pdo->prepare($sql);
            $items = [];

            if ($prep) {
                $res = $prep->execute([ $categoryId, $cashboxId ]);

                if ($res) {
                    $items = $prep->fetchAll(PDO::FETCH_OBJ);
                }
            }

            return $items;
        }

        public function getRecordsByTimeScope(int $cashboxId, string $recordCreatedFrom, string $recordCreatedTo): array {
            $pdo = $this->getDatabaseConnection()->getConnection();
            $sql = 'SELECT * FROM record WHERE cashbox_id=? and created_at>=? and created_at<=?;';
            $prep = $pdo->prepare($sql);
            $items = [];

            if ($prep) {
                // $res = $prep->execute([ $cashboxId, $recordCreatedFrom, $recordCreatedTo]);
                $res = $prep->execute([ $cashboxId, $recordCreatedFrom, $recordCreatedTo]);

                if ($res) {
                    $items = $prep->fetchAll(PDO::FETCH_OBJ);
                }
            }

            return $items;
        }

        // public function getAllByCashboxId(int $cashboxId): array {
        //     $pdo = $this->getDatabaseConnection()->getConnection();
        //     $sql = 'SELECT * FROM record WHERE cashbox_id=? ORDER BY created_at DESC;';
        //     $prep = $pdo->prepare($sql);
        //     $items = [];

        //     if ($prep) {
        //         $res = $prep->execute([ $cashboxId ]);

        //         if ($res) {
        //             $items = $prep->fetchAll(PDO::FETCH_OBJ);
        //         }
        //     }

        //     return $items;
        // }
    }
