<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use \PDO;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class CashboxModel extends Model {
        protected function getFields() {
            return [
                'cashbox_id'     => new Field(
                                        (new NumberValidator())
                                            ->setInteger()
                                            ->setUnsigned()
                                            ->setMaxIntegerDigits(11), false),

                'created_at'     => new Field(new DateTimeValidator(), false),

                'name'          => new Field(
                                        (new StringValidator())
                                            ->setMinLength(1)
                                            ->setMaxLength(255)),

                'description'    => new Field(
                                        (new StringValidator())
                                            ->setMinLength(1)
                                            ->setMaxLength(64000)),

                'currency'       => new Field(
                                        (new StringValidator())
                                            ->setMinLength(3)
                                            ->setMaxLength(3)),
                                            
                'saldo'          => new Field(
                                        (new NumberValidator())
                                            ->setUnsigned()
                                            ->setMaxDecimalDigits(2)),

                'user_id'        => new Field(
                                        (new NumberValidator())
                                            ->setInteger()
                                            ->setUnsigned()
                                            ->setMaxIntegerDigits(11)),
                
                'priority'        => new Field(
                                        (new NumberValidator())
                                            ->setInteger()
                                            ->setMaxIntegerDigits(11)),
                
            ];
        }

        public function getAllCategoriesByUserId(int $useryId): array {
            return $this->getAllByFieldName('user_id', $useryId);
        }
    
        public function getAllCategoriesByUserIdFun(int $useryId): array {
            $pdo = $this->getDatabaseConnection()->getConnection();
            $sql = 'SELECT  `cashbox`.`cashbox_id`, `cashbox`.`priority`, `cashbox`.`name`, `cashbox`.`description`, `cashbox`.`saldo`, `cashbox`.`currency`, `cashbox`.`created_at`, 
                    MAX(`record`.`created_at`)  as record_created_at, record.cash, record.category_id, 
                    `category`.`category_type`, `category`.`name` as category_name 
                    FROM cashbox 
                        LEFT JOIN record ON `cashbox`.`cashbox_id` = `record`.`cashbox_id` 
                        LEFT JOIN category ON `category`.`category_id`= `record`.`category_id` 
                    WHERE cashbox.user_id=? GROUP BY `cashbox`.`cashbox_id` ORDER BY `cashbox`.`priority` DESC;';
            $prep = $pdo->prepare($sql);
            $items = [];

            if ($prep) {
                // $res = $prep->execute([ $cashboxId, $recordCreatedFrom, $recordCreatedTo]);
                $res = $prep->execute([ $useryId]);

                if ($res) {
                    $items = $prep->fetchAll(PDO::FETCH_OBJ);
                }
            }

            return $items;
        }
        
        public function getAllPrioritiesByUserId(int $useryId): array {
            $pdo = $this->getDatabaseConnection()->getConnection();
            $sql = 'SELECT  `cashbox`.`cashbox_id`, `cashbox`.`priority`,`cashbox`.`name`, `cashbox`.`description`, `cashbox`.`saldo`, `cashbox`.`currency`, `cashbox`.`created_at`, 
                    MAX(`record`.`created_at`)  as record_created_at, record.cash, record.category_id, 
                    `category`.`category_type`, `category`.`name` as category_name 
                    FROM cashbox 
                        LEFT JOIN record ON `cashbox`.`cashbox_id` = `record`.`cashbox_id` 
                        LEFT JOIN category ON `category`.`category_id`= `record`.`category_id` 
                    WHERE cashbox.user_id=? GROUP BY `cashbox`.`cashbox_id` ORDER BY `cashbox`.`priority` DESC;';
            $prep = $pdo->prepare($sql);
            $items = [];

            if ($prep) {
                // $res = $prep->execute([ $cashboxId, $recordCreatedFrom, $recordCreatedTo]);
                $res = $prep->execute([ $useryId]);

                if ($res) {
                    $items = $prep->fetchAll(PDO::FETCH_OBJ);
                }
            }

            return $items;
        }

        public function getPriorityByUserId(int $cashboxId, int $useryId){
            $pdo = $this->getDatabaseConnection()->getConnection();
            $sql = 'SELECT  `cashbox`.`priority` FROM cashbox 
                    WHERE cashbox.user_id=? and cashbox.cashbox_id=?;';
            $prep = $pdo->prepare($sql);
            $items = [];

            if ($prep) {
                // $res = $prep->execute([ $cashboxId, $recordCreatedFrom, $recordCreatedTo]);
                $res = $prep->execute([ $useryId, $cashboxId ]);

                if ($res) {
                    $items = $prep->fetch(PDO::FETCH_OBJ);
                }
            }

            return $items;
        }
    }


    